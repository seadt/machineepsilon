﻿static void machineEpsilon()
{
    double eps = 1;      //pozitif bir sayı seçilir (1+eps)
    while ((1+eps)>1)
    {
        eps = eps / 2;   //1'den büyük kalacak fakat tanınmayacak hale gelene kadar 2'ye bölünür

    }
    eps = eps * 2;       //döngüden çıkınca da 2 ile çarpıp makine epsilonunu buluruz
    Console.WriteLine("Bu bilgisayarın makine epsilonu " + eps + "'dir");
}
machineEpsilon();